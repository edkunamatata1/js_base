# JavaScript Base

My take on a starter kit for JS development. Just copy and run!

## Get Started

## Install from Scratch

1. **Install editor of choice**
    
    Recommended for JS:
    - VS Code
    - Webstorm
    - Sublime Text

2. **Install [Node Version Manager](https://github.com/creationix/nvm)**

    Working on Windows? Use [nvm-windows](https://github.com/coreybutler/nvm-windows)

3. **Install [Node 6 or newer](https://nodejs.org/en/download/releases/)**

    ```shell
    > cd c:
    > nvm install latest
    > nvm use <version>
    > node -v
    ```

4. **Create a root folder for the new project and cd there**

    ```shell
    > cd <project root>
    ```

5. **Clone this repository**

    ```shell
    > git clone https://edkunamatata1@bitbucket.org/edkunamatata1/js_base.git
    ```

6. **Install node packages**

    ```shell
    > npm install
    ```

7. **Install VS plugins (for VS users)**

    - EditorConfig for VS Code
    - TODO Highlight

8. **Disable 'safe write' on you editor**

    - Sublime Text 3: `Add atomic_save: 'false'` to your user preferences
    - JetBrains IDEs (e.g. WebStorm): Uncheck "Use safe write" in Preferences > Appearance & Behavior > System Settings.
    - Vim: Add `:set backupcopy=yes` to your settings.



## Running

1. **Run the app.**
    This will run the automated build process, start up a webserver, and open the application in your default browser. When doing development with this kit, this command will continue watching files all your files. Every time you hit save the code is rebuilt, linting runs, and tests run automatically. Note: The -s flag is optional. It enables silent mode which suppresses unnecessary messages during the build.

    ```shell
    > npm develop
    ```

2. **Start the JSON Server**
    ```shell
    json-server --watch db.json
    ```


## Technologies
| **Tech** | **Use** | **Learn More** |
| --- | --- | --- |
| [node](https://nodejs.org)                         | package manager |
| [Express]()           | backend |
| [React]()             | frontend |
| [MongoDB]()           | database |
| [Babel](http://babeljs.io)                         | transpiling |
| [Webpack](https://webpack.js.org)                  | bundling | [short howto](https://github.com/petehunt/webpack-howto) |
| [SASS](http://sass-lang.com/)                      | css utility |
| [ESLint](http://eslint.org/)                       | linting |
| [Editor Config](http://editorconfig.org)           | formatting |
| [npm Scripts](https://docs.npmjs.com/misc/scripts) | build automation |

---
## Other info

### Steps to create .babelrc file on Windows
1. Press Windows + R to spawn run app
2. type 'notepad .babelrc' and run
3. Click continue at the file does not exist prompt

### W4 
Reasonable defaults: webpack 4 main concepts are entry, output, loaders, plugins. I will not cover these in details, although the difference between loaders and plugins is very vague. It all depends on how library author has implemented it.
